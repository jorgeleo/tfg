//La forma d'escriure el text serà via scr_text("text",speed,x,y);  
//A partir de la variable txt donarem la informació de personalització del text

var txt = instance_create(argument2, argument3, obj_text);
with(txt){
    text = argument0;
    maxlength = 400;
    padding = 15;
    spd = argument1;
    font = font_dialogue;
    draw_set_font(font)
    text_length = string_length(text);
    font_size = font_get_size(font);
    text_width = string_width_ext(text, font_size+(font_size/2), maxlength);
    text_height = string_height_ext(text, font_size+(font_size/2), maxlength);
    boxwidth = text_width + (padding*2);
    boxheight = text_height + (padding*2);
    
}
