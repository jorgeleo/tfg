//Definim les variables que interpreten l'estima dels personatges per la protagonista
// scr_opinion(persona, valor)
var op_maria = 0;
var op_marco = 0;
var op_andrea = 0;
var op_fran = 0;

var persona = argument0;
var valor = argument1;

switch (persona){
    case maria:
        opinion = op_maria + valor;
        op_maria = opinion;
        break;
    case marco:
        opinion = op_marco + valor;
        op_marco = opinion;
        break;
    case andrea:
        opinion = op_andrea + valor;
        op_andrea = opinion;
        break;
    case fran:
        opinion = op_fran + valor;
        op_fran = opinion;
        break;
}
